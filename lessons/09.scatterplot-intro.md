## Tying SVG into D3js
Over the next lessons we're going to look at several convenient, and frankly, necessary components that D3js offers which provide clarity to the data we're trying to visualize.  We'll use our SVG skills from the previous lessons to our advantage and incorporate axes and scale our data.  Our aim is to build a scatterplot of circles with the use of SVG and D3js to control the position and size of the circles.  To give the proper context, we'll also build out axes.

![Scatterplot](../images/scatterplot.PNG)

Let's take a moment to point out a few things. 

1. The black border is our SVG canvas.
2. The x and y axes are *scaled* to the dimensions of the SVG canvas and we control how many 'ticks' we want to display.
3. What isn't noticeable is that there are margins in play here.  Not our traditional CSS margins, but our own margins between the SVG canvas border and where our axes are plotted.
4. The circles' coordinates are also affected by these margins.
5. There's going to be some basic math involved.

Much of our D3js journey is built around exploration and curiosity, but we have some ideas on how best to start with something complex like this.  First, let's set up some basics, such as how large we want our canvas to be.  Second, we need to make sure there's enough room around the edges of the canvas to fit our axes in there along with all their information.  We haven't done anything like this so far with our bar charts.  Third, we're going to need to be able to scale our data and axes across the entire remaining width and height after we subtract any margins. Lastly, we'll need to use D3js to expose our data and use it to help us build circles.

If that seems like a lot of tasks, it is.  A good way to get around all these details is to write them down and use them whenever we need them again.  For example, our SVG width and height.  Much of our math is going to center around these dimensions as well as the space we want to have around the edges.

`scatter.js`
```javascript
var width = 800;
var height = 500;
var margins = { 
    left: 30,
    right: 30,
    top: 30,
    bottom: 40
};
```

What we have in mind is the following picture.

![Margins](../images/ScatterplotMargins.png)

Combining what we learned about plotting circles with SVG in the last assignment, we know that everything stems from the origin (0,0) being in the top left corner of our canvas, but now we have a top and left margin buffering that, so we have to keep this in mind when we plot our circles.

But first, let's get into scaling and axes and take a look at our sample data.

`scatter.js`
```javascript
// ...
var circleData = [
    {
        x: 7,
        y: 4,
        radius: 4
    },
    {
        x: 2,
        y: 8,
        radius: 8
    },
    {
        x: 8,
        y: 2,
        radius: 5
    },
    {
        x: 3,
        y: 3,
        radius: 2
    },
    {
        x: 6,
        y: 8,
        radius: 1
    },
    {
        x: 1,
        y: 1,
        radius: 5
    },
];
```

The data above is rather simple and to the point.  Each object has an `x` and `y` coordinate, plus a `radius` that will help us plot and size our circles.  Our data suggests that every set of coordinates fits in a 10x10 grid.  We'll use that assumption to help us with the next part: determining scales and axes.  

## Scales

Scales help in the same way we think about scaling applications or businesses.  "If we had 500 customers, how would we handle that?" or "If we had 1,000 users coming to our site per hour, how do we scale appropriately?".  We want to use the width and height of our SVG canvas and trim out the margins to scale our data against.  Our width is 800px and our left and right margins are 30, so we're left with 740px to work with in between.

As expected, D3js has built-in ways to accommodate our need to stretch our data across the canvas evenly.  We will be picking out a linear-based scale from the [API Docs](https://github.com/d3/d3/blob/master/API.md#scales-d3-scale) `scaleLinear()`.  What's great about this is that everything about the scale gets initialized and can be customized as we see fit.  Let's check out a couple more pieces to the puzzle.

## Domain & Range 

`.domain().range()` are two important methods we can use to set the domains and ranges of any scales we create.  Think of the domain as the hard values our data holds.  In this case, we're expecting our data to be within the domain of 0 and 10.  For the range, think of it as the canvas we're trying to stretch our domain across. Below is the domain and ranges of the width of our scatterplot.

![Domain & Range](../images/DomainRange.png)

In code, we chain the methods together and provide arrays to the domain and range methods.  The x-axis has a domain of 0 to 10 and range of 0 to 800.  We can expect then that an x-coordinate of 5 would find its way to the middle of the range which is 400.  

The other beautiful part of ranges is that we can invert them.  Remember our problem with the origin being top left? Well when we were growing up, our origins tended NOT to be there whenever we drew graphs, so it's not easy to think about positive values moving downward and visa versa.  

To remedy this, let's look at the y-axis in particular.  It has a domain of 0 to 10, but we want that to be applied to a range that goes from the height of our canvas minus the top and bottom margins, i.e. 500-30-40=430. Now our range looks like [430, 0] where if a y-coordinate is 2, then we can expect it to be placed 86 pixels up from the bottom because...

- 430 pixels is the range difference
- The domain has a difference of 10
- Every 1 unit in the domain translates to 43 pixels in the range

![Domain Range Inverted](../images/DomainRangeInverted.png)

`scatter.js`
```javascript
// ...
var xscale = d3.scaleLinear()
                .domain([0,10])
                .range([0,width-margins.left-margins.right]);

var yscale = d3.scaleLinear()
                .domain([0,10])
                .range([height-margins.top-margins.bottom,0]);
```

After learning about scales, domains and ranges, we're ready to learn about building our axes in the next lesson.

[Next Lesson --->](https://gitlab.com/TEnders64/academy-d3js/blob/master/lessons/10.assignment-scatterplot-final.md)