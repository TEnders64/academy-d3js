// // We've been trying to UPDATE all along! 
// // Because we're trying to select all existing divs inside the body
// // and tie data to them
// var bars = d3.select("body")
//             .selectAll("div")
//             .data([4, 8, 15, 16, 20, 12])
//             // We create the bars variable to hang onto our previous d3 selection
//             // and preserve the data/elements that we console logged previously.
//             // We don't want to lose the _enter and _exit collections. As soon as we execute .data()
//             // the _enter and _exit collections are determined.
            
//             // Now let's ENTER and create the right amount of divs to supplement our data
//             // This will append to the body element since we saved the selection previously into
//             // the bars variable
//             bars.enter().append("div")
//             .style("background-color","red")
//             .style("height", function(d){ return d*20+"px"; })
            
//             // Now let's EXIT and remove any lingering divs we might have had
// // In this case we had no surplus of divs to remove. We will cover this with examples later.
// bars.exit().remove();

var bars = d3.select("#chart")
    .selectAll("div")
    .data([4, 8, 15, 16, 20, 12])

bars.enter().append("div")
    .text(function(d){ return d; })
    .style("height", function(d){ return d*20+"px" })

// console.log(d3.select("body")
//             .selectAll("div")
//             .data([24,16,18,12,22,16]));

function randomsGenerator(){
    var nums = [];
    var size = Math.floor(Math.random()*20);
    for (var i = 0; i < size; i++){
        nums.push(Math.floor(Math.random()*20));
    }
    console.log("New Nums: "+nums);
    buildChart(nums);
}

function buildChart(data){
    var bars = d3.select("#chart")
        .selectAll("div")
        .data(data)
        
    bars.transition()
        .duration(2000)
        .text(function(d){ return d; })
        .style("height", function(d){ return d*20+"px" })
        .style("background-color", "gray")

    console.log(bars);

    bars.enter().append("div")
        .transition()
        .duration(2000)
        .text(function(d){ return d; })
        .style("height", function(d){ return d*20+"px" })
        .style("background-color", "lightblue")

    bars.exit().remove();
}