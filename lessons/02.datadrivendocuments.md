#### Data-Driven Documents and what that means
As mentioned in our welcome, Data-Driven Documents, where D3js' name originates from, is a novel idea that means our documents, aka webpages, are centered around data!

How does that look? Imagine on the previous page behind each of those bars we had data bound to them and influencing their shape, specifically their heights.  For visual purposes, think of the image below.

![Bar Chart](../images/BarChartWithDataAxes.png)

Now how might we tie the bars and data together? HTML elements, which are often referred to as nodes in [D3js Documentation](https://github.com/d3/d3/wiki), are tied to data using some pretty interesting methods. Let's take a look at some D3js code and very basic HTML.

`index.html`
```html
<html>
    <head>
        <title>Dummy HTML</title>
        <script src="https://d3js.org/d3.v5.js"></script>
    </head>
    <body>
        <!-- we want our bars to show up here eventually -->
    </body>
    <script src="bars.js"></script>
</html>
```
`bars.js`
```javascript
d3.select("body")
  .selectAll("div")
  .data([24,16,18,12,22,16])
```

Let's point out the simpler pieces first. 
1. `d3` is the exposed object that we'll begin our HTML selections with.
2. The `.select()` method works as expected, selecting single or multiple elements just like the way your W3C Selectors work normally within Javascript.
3. It seems like the `.data()` method is supplying our data, but the `.selectAll()` method is the odd one out.

Why is there a `.select()` and a `.selectAll()` method chained together?  The first `.select()` chooses an existing element in our HTML that we'd like to add content to as you'd expect, but the `.selectAll()` is us signaling to D3js to prepare to build us `div` elements.  How many though? As many as is necessary to satisfy the `.data()` method's input.  In this case, 6 divs are prepared to be created! We'll discuss the nuance of this in the next lesson.

It may seem odd, but this is our first exposure to the way D3js operates.  We tell D3js what elements we want built **FIRST**, then we supply our data that will be bound to those elements.  To show off this feat, if we log the following...

```javascript
console.log(d3.select("body")
            .selectAll("div")
            .data([4, 8, 15, 16, 20, 12]));
```
...what we see below is just scratching the surface, but will help you later understand how the DOM is manipulated by D3js in similar ways that jQuery might.

![Enter, Exit, and More](../images/FirstConsoleLog.PNG)

If you expand the `_enter` Array, you will see that there are 6 instances of EnterNodes ready to populate, and if you check each of them, you'll find a `__data__` property.  So if you're curious about how and where this information is being stored, now you know!

Let's dive into an example of how we use that data to our advantage in the next set of lessons where we build out a static bar chart.

[Next Lesson --->](https://gitlab.com/TEnders64/academy-d3js/blob/master/lessons/03.barchart-introtojoins.md)